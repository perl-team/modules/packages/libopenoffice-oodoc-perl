Source: libopenoffice-oodoc-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Damyan Ivanov <dmn@debian.org>,
           gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libarchive-zip-perl,
                     libxml-twig-perl,
                     perl
Standards-Version: 3.9.5
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libopenoffice-oodoc-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libopenoffice-oodoc-perl.git
Homepage: https://metacpan.org/release/OpenOffice-OODoc

Package: libopenoffice-oodoc-perl
Architecture: all
Depends: libarchive-zip-perl,
         libxml-twig-perl,
         ${misc:Depends},
         ${perl:Depends}
Recommends: libtext-wrapper-perl
Description: module for working with Open Document Format files
 OpenOffice::OODoc is a Perl module for reading from/writing to files that
 comply with the OASIS Open Document Format for Office Applications (ODF),
 also known as the ISO/IEC 26300:2006 standard. It provides a high-level,
 document-oriented language, and isolates the programmer from the details
 of the file format.
 .
 This module can process different document classes (texts, spreadsheets,
 presentations, and drawings). It can retrieve or update styles and images,
 document metadata, as well as text content.
